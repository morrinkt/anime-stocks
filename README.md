# README #

### What is this repository for?

An application to track user participation/ratings for foreign animated shows over extended periods of times across multiple websites. It is collected and displayed in a fashion similar to financial stocks and price charts. The websites include:

* myanimelist.net (MAL)
* anidb.net (ADB)
* animenewsnetwork.com (ANN)

### Version

0.0.2, Alpha (22 Sept 2014)

### Dependencies

+ `malparser` MAL parser module WITH CUSTOM CODE, included in repo source ----- (github.com/JohnDoee/web-parsers/tree/master/myanimelist/)
+ `anidbparser` AniDB parser module WITH CUSTOM CODE, included in repo source ----- (github.com/iamale/anidb.py)
+ `selenium` `webdriver` module ----- (docs.seleniumhq.org/projects/webdriver/)
+ `lxml` xml parser module ----- (lxml.de)
+ `bs4` BeautifulSoup html parser module ----- (www.crummy.com/software/BeautifulSoup/)

### Admin

morrinkt@yahoo.com, bseid94@gmail.com