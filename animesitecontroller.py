# XML PROCESSING MODULES
import requests # HTTP GET request module
import xml.etree.ElementTree as ET # XML parse module
import re # String replacement module

# HTML PROCESSING MODULES
from bs4 import BeautifulSoup
# MyAnimeList html parse module (CUSTOM BUILD)
# https://github.com/JohnDoee/web-parsers/tree/master/myanimelist/
from malparser import MAL

# AniDB parse module (CUSTOM BUILD)
# https://github.com/iamale/anidb.py
from anidbparser.anidb import AniDB

# Web browser emulation package
from selenium import webdriver
# Variable to activate selenium emulated browser throughout code (True/False)
BROWSER_ACTIVE = True

# OTHER MODULES
import sys
import os.path, time
from time import sleep

class ASC():
	'''
	ASC - Anime Site Controller class.
	
	This class represents an object that fetches, parses and returns information from
	each of the major supported anime database websites:
		MyAnimeList - MAL (myanimelist.net)
		AniDB - ADB (anidb.net)
		AnimeNewsNetwork - ANN (animenewsnetwork.com)
		
	Main variables of ASC object:
		self.browser - Emulated browser object
		self.mal - MAL parser object
		self.anidb - AniDB parser object
		self.headers - HTTP headers used for sending custom user-agent
		
	AUTHOR: Bobby M. Seid
	DATE: 22 Sept 2014
	VERISON: 0.0.2 (ALPHA)
	'''
	# Static class variables used for function inputs
	MAL = 10 # MyAnimeList
	ADB = 20 # AniDB
	ANN = 30 # AnimeNewsNetwork
	PIC = 40 # Picture (AniDB)
	RATINGS_REPORT = 'ratings.xml'
	USER_AGENT = 'api-indiv-E1751B9AA0959370CFDBEEBFD125A27D'
	
	# *************** INITIALIZATION/CLEANUP FUNCTIONS ***************
	
	def __init__(self):
		'''
		AnimeSiteController (ASC) class constructor.
		'''
		global BROWSER_ACTIVE
		if (BROWSER_ACTIVE):
			# Creates an emulated browser (Selenium) for HTML fetching
			self.browser = webdriver.Firefox()
		
		# Creates and opens the MAL parser
		self.mal = MAL()
		
		# Connect to AniDB parser client
		client_id = 'animestocks'
		client_version = 2
		self.anidb = AniDB(client_id,client_version)
		
		# Fetch daily ANN ratings report, if old
		now = time.time()
		fileModified = os.path.getctime(ASC.RATINGS_REPORT)
		expiration = now - 60*60*12
		if (fileModified < expiration):
			self._get_full_report_ANN(ASC.RATINGS_REPORT)
		
		# Set HTTP header
		self.headers = {
			'User-Agent': ASC.USER_AGENT,
			'From': 'morrinkt@yahoo.com'
		}
		
	if (BROWSER_ACTIVE):
		def cleanup(self):
			'''
			Method called after ASC object is finished being used.
			'''
			# Closes the emulated browser (Selenium)
			self.browser.close()
	
	# *************** GENERAL FUNCTIONS ***************
	
	def get_anime(self,id,database):
		'''
		Get information for a specific anime from all databases:
			MAL - Name, MAL_ID, Users, Score
			AniDB - Name, ANIDB_ID, Weighted (rating), Average (temporary)
			ANN - Name, ANN_ID, Weighted (score)
		
		Keyword arguments:
			id -- int; Specific anime identification number for a database
			database -- int; Anime database to be searched. Can also retrieve picture.
				ASC.MAL = MyAnimeList
				ASC.ADB = AniDB
				ASC.ANN = AnimeNewsNetwork
				ASC.PIC = Get picture
				
		Return:
			list
			None; error happened or no show was found for the ID entry
		'''
		if (database == ASC.MAL):
			return self._get_MAL(id)
		elif (database == ASC.ADB):
			return self._get_AniDB(id)
		elif (database == ASC.ANN):
			return self._get_ANN(id)
		elif (database == ASC.PIC):
			return self._picture_AniDB(id)
		else:
			raise
		
	def search_anime(self,showname,database):
		'''
		Search all databases for information on an anime. Returns a list of names and IDs with
		names similar to the requested name from each database.
		
		Keyword arguments:
			showname -- string; Name of show that will be searched. Multiple entries may be found.
			database -- int; Anime database to be searched.
				MAL = MyAnimeList
				ADB = AniDB
				ANN = AnimeNewsNetwork
			
		Return:
			list; an array of elements each containing an entry: {'Name': string,'ID': int}
			None; error happened or no show was found for the showname
		'''
		if (database == ASC.MAL):
			return self._search_MAL(showname)
		elif (database == ASC.ADB):
			return self._search_AniDB(showname)
		elif (database == ASC.ANN):
			return self._search_ANN(showname)
		else:
			raise
		
	# *************** ENCAPSULATED FUNCTIONS FOR EACH DATABASE ***************
	
	def _get_MAL(self,id):
		'''
		PRIVATE FUNCTION
		
		Get MAL (MyAnimeList.net) information for a specific anime.
		
		Keyword arguments:
			id -- int; Specific MAL identification number.
			
		Return:
			entry; an array of elements containing information about a show:
				{'Name': string,'ID': int,'Score': float,'Users': int}
			None; error happened or no show was found for the ID entry
		'''
		anime = self.mal.get_anime(id)
		try:
			anime.fetch()
		except:
			return None
		
		name = anime.title
		score = float(anime.statistics['Score'])
		users = anime.statistics['Members']
		
		entry = {'Name': name,'ID': id,'Score': score,'Users': users}
		return entry
		
	def _search_MAL(self,showname):
		'''
		PRIVATE FUNCTION
		
		Search MAL (MyAnimeList.net) information for an anime. Returns a list of names and IDs with
		names similar to the requested name.
		
		Keyword arguments:
			showname -- string; Name of show that will be searched. Multiple entries may be found.
			
		Return:
			list; an array of elements each containing an entry: {'Name': string,'ID': int}
			None; error happened or no show was found for the showname
		'''
		# Replace spaces in showname with "+" as per HTML encoding standards
		s = showname
		s = s.replace(" ","+")
	
		# Get XML file (as string) from MAL
		url = 'http://myanimelist.net/api/anime/search.xml?q=' + s
		user = 'morrinkt'
		pw = 'rig6iq'
		r = requests.get(url,auth=(user,pw),headers=self.headers)
		root = self._parse_xml(r)
		if not root:
			return None
	
		# Get information from XML file
		list = []
		for show in root.findall('entry'):
			name = show.find('title').text
			id = show.find('id').text	
			entry = {'Name': name,'ID': id}
			list.append(entry)
			
		# Check if list is empty, otherwise return list
		if not list:
			return None
		else:
			return list
		
	def _get_AniDB(self,id):
		'''
		PRIVATE FUNCTION
		
		Get AniDB (anidb.net) information for a specific anime.
		
		Keyword arguments:
			id -- int; Specific AniDB identification number.
			
		Return:
			entry; an array of elements containing information about a show:
				{'Name': string,'ID': int,'Weighted': float,'Average': float}
			None; error happened or no show was found for the ID entry
		'''
		# msg = 'AUTH user=morrinkt&pass=rig6iq&protover=3&client=animestocksudp&clientver=100&tag=login'
		# server = 'api.anidb.net'
		# port = 9000
		
		
		global BROWSER_ACTIVE
		# Use this code if emulated browser is required
		if (BROWSER_ACTIVE):
			url = 'http://anidb.net/perl-bin/animedb.pl?show=anime&aid=' + str(id)
			self.browser.get(url)
			html = self.browser.page_source
			soup = BeautifulSoup(html)
			if soup.error is not None:
				raise Exception(soup.error.string)
				
			list = []
			link = 'animedb.pl?show=votes&aid=' + str(id)
			for element in soup.find_all('a'):
				if (element.get('href') == link):
					try:
						list.append(float(element.string))
					except:
						pass
					
			for element in soup.find_all('h1'):
				name = element.string.replace('Anime: ','')
			
			try:
				entry = {'Name': name,'ID': id,'Weighted': list[0],'Average': list[1]}
			except:
				entry = {'Name': name,'ID': id,'Weighted': 0.00,'Average': 0.00}
		# Use this code if AniDB HTTP API is required
		else:
			try:
				anime = self.anidb.get(id)
				name = anime.title
				id = anime.id
				weighted = anime.ratings.permanent # weighted rating
				average = anime.ratings.temporary # average
				entry = {'Name': name,'ID': id,'Weighted': weighted,'Average': average}
			except:
				print('***BLOCKED***')
				entry = {'Name': None,'ID': id,'Weighted': 0.00,'Average': 0.00}
		
		# entry = {'Name': name,'ID': id,'Weighted': weighted,'Average': average}
		return entry
		
	def _picture_AniDB(self,anidb_id):
		'''
		PRIVATE FUNCTION
		
		Return the HTTP link to the jpg/png picture used to represent the given anime.
		
		Keyword arguments:
			anidb_id -- int; Specific AniDB identification number.
			
		Return:
			string; HTTP link to the jpg/png picture
		'''
		anime = self.anidb.get(anidb_id)
		return anime.picture
		
	def _search_AniDB(self,showname):
		'''
		PRIVATE FUNCTION
		
		Search AniDB (anidb.net) information for an anime. Returns a list of names and IDs with
		names similar to the requested name.
		
		Keyword arguments:
			showname -- string; Name of show that will be searched. Multiple entries may be found.
			
		Return:
			list; an array of elements each containing an entry: {'Name': string,'ID': int}
			None; error happened or no show was found for the showname
		'''
		tmp = self.anidb.search(showname)
		
		# Get information from search
		list = []
		for show in tmp:
			name = show.title
			id = show.id
				
			entry = {'Name': name,'ID': id}
			list.append(entry)
		
		# Check if list is empty, otherwise return list
		if not list:
			return None
		else:
			return list
		
	def _get_ANN(self,id):
		'''
		PRIVATE FUNCTION
		
		Get ANN (animenewsnetwork.com) information for a specific anime.
		
		Keyword arguments:
			id -- int; Specific MAL identification number.
			
		Return:
			entry; an array of elements containing information about a show:
				{'Name': string,'ID': int,'Weighted': float}
			None; error happened or no show was found for the ID entry
		'''
		id = int(id)
		root = self._parse_xml(None,None,'ratings.xml')
		if not root:
			return None
		
		for item in root.findall('item'):
			if (int(item.get('id')) == id):
				name = item.find('anime').text
				weighted = float(item.find('weighted_average').text)
				entry = {'Name': name, 'ID': id, 'Weighted': weighted}
				return entry
				
		return None # Only reach here if ID not found in list
		
	def _get_full_report_ANN(self,filename):
		'''
		PRIVATE FUNCTION
		
		Get the DAILY, full xml "ratings" repot from ANN (animenewsnetwork.com).
		http://www.animenewsnetwork.com/encyclopedia/reports.xml?id=172&type=anime&nlist=all
		
		Keyword arguments:
			filename -- string; name of file for ratings report to be written to
			
		Return:
			None
		'''
		# Get XML file (as string) from ANN
		url = 'http://www.animenewsnetwork.com/encyclopedia/reports.xml?id=172&type=anime&nlist=all'
		r = requests.get(url)
		root = self._parse_xml(r)
		if not root:
			return None
		tree = ET.ElementTree(root)
		tree.write(filename)

	def _search_ANN(self,showname):
		'''
		PRIVATE FUNCTION
		
		Search ANN (animenewsnetwork.com) information for an anime. Returns a list of names and IDs with
		names similar to the requested name.
		
		EXAMPLE: http://cdn.animenewsnetwork.com/encyclopedia/api.xml?anime=~hitsugi
		
		Keyword arguments:
			showname -- string; Name of show that will be searched. Multiple entries may be found.
			
		Return:
			list; an array of elements each containing an entry: {'Name': string,'ID': int}
			None; error happened or no show was found for the showname
		'''
		# Replace spaces in showname with "+" as per HTML encoding standards
		s = showname
		s = s.replace(" ","+")
	
		# Get XML file (as string) from ANN
		url = 'http://cdn.animenewsnetwork.com/encyclopedia/api.xml?anime=~' + s
		r = requests.get(url)
		root = self._parse_xml(r)
		if not root:
			return None
	
		# Get information from XML file
		list = []
		for show in root.findall('anime'):
			name = show.get('name')
			id = show.get('id')
			entry = {'Name': name,'ID': id}
			list.append(entry)
		
		# Check if list is empty, otherwise return list
		if not list:
			return None
		else:
			return list

	# *************** OTHER FUNCTIONS ***************
		
	def _parse_xml(self,request=None,string=None,xmlfile=None):
		'''parse_xml([request[, string[,xmlfile]]]) - parse an xml from string or .xml file.'''
		if not request and not string and not xmlfile:
			return None
		
		# Parse from Requests object
		if request:
			# Try parsing XML text as string
			if (request.status_code == 200):
				try:
					t = request.text
					t = re.sub('&.*;','',t) # Delete all broken XML/HTML tags (&?????;)
					root = ET.fromstring(t)
					return root
				except IOError as e:
					# print("[XML] I/O Error " + e.errno + ": " + e.strerror)
					return None
				except ET.ParseError as e:
					# print("[XML] Parse Error.")
					# print(e.position)
					# print(e.code)
					return None
				except:
					# print("[XML] Unexpected error.")
					# print(sys.exc_info())
					return None
			elif (request.status_code == 204):
				# print('No XML content found.')
				return None
			else:
				# print('Error in completing HTTP GET request.')
				return None
		
		# Parse from string
		if string:
			# Try parsing XML text as string
			try:
				t = re.sub('&.*;','',string) # Delete all broken XML/HTML tags (&?????;)
				root = ET.fromstring(t)
				return root
			except IOError as e:
				# print("[XML] I/O Error " + e.errno + ": " + e.strerror)
				return None
			except ET.ParseError as e:
				# print("[XML] Parse Error.")
				# print(e.position)
				# print(e.code)
				return None
			except:
				# print("[XML] Unexpected error.")
				# print(sys.exc_info())
				return None
			return None
			
		# Parse from .xml file
		if xmlfile:
			try:
				tree = ET.parse(xmlfile)
				root = tree.getroot()
				return root
			except:
				return None
	
	def _send(self,data, port=9000, addr='api.anidb.net'):
		'''send(data[, port[, addr]]) - multicasts a UDP datagram.'''
		# Create the socket
		s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		# Connect to socket
		s.connect((addr,port))
		# Send the data
		s.sendto(bytes(data), (addr, port))
		
	def _recv(self,port=9000, addr='api.anidb.net', buf_size=1024):
		'''recv([port[, addr[,buf_size]]]) - waits for a datagram and returns the data.'''
		
		# Create the socket
		s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		
		# Bind to the port
		s.bind(('',port))
		
		# Receive the data, then unregister multicast receive membership, then close the port
		data, sender_addr = s.recvfrom(buf_size)
		s.close()
		return data
	
	def _debug(self):
		'''
		Debugging function.
		
		THIS FUNCTION IS FOR TESTING PURPOSES ONLY.
		'''
		end = 0

