# INTERNAL APPLICATION MODULES
from animesitecontroller import ASC

# OTHER MODULES
import csv
import time
from random import randint
import re # String replacement module
import urllib.request
import os.path

class DC():
	'''
	DC - Data Controller class.
	
	This class represents an object that reads, writes and moves data for
	analysing anime rating trends.
	
	AUTHOR: Bobby M. Seid
	DATE: 22 Sept 2014
	VERSION: 0.0.2 (ALPHA)
	
	#TODO
	1) Create ID adding method for multiple inquiries.
	'''
	
	# Static class variables used for function returns (DC.OK is optional)
	ERROR = -200
	INVALID = -100
	OK = 100
	
	SLEEP_TIME = 20
	
	SYMBOL_ID_LIST = 'ID_list.csv'
	SYMBOL_LIST_DEFAULT = 'symbols.txt'
	PIC_FILEPATH_DEFAULT = 'pictures\\'
	SEASONS = ['Winter','Spring','Summer','Fall']
	
	def __init__(self):
		'''
		DataController (DC) class constructor.
		'''
		self.asc = ASC()
		
	def iterateSymbols(self,symbol_list=None):
		'''
		Writes data for each stock symbol for today's date into their respective *.csv files.
		
		Keyword arguments:
			symbol_list -- string; filepath of *.txt file where symbols are stored. Default is used if None given.
			
		Return:
			DC.ERROR -- int; An exception has occurred within this function.
		'''
		if not symbol_list:
			filepath = DC.SYMBOL_LIST_DEFAULT
		else:
			filepath = symbol_list
			
		try:
			with open(filepath) as f:
				for line in f:
					symbol = line.rstrip()
					data = self._getData(symbol)
					self._writeLine(symbol,data)
					rand_time = randint(1,40) # Add random time to avoid scrape bans
					time.sleep(DC.SLEEP_TIME+rand_time)
		except:
			raise
			
	def savePicture(self,symbols,filepath=None):
		'''
		Downloads and saves the presentation picture associated with an anime show. Pictures are downloaded to filepath.
		
		Keyword arguments:
			symbols -- list; A list of string symbols.
			filepath -- string; Filepath to store pictures. If None, default is used.
			
		Return:
			None
		'''
		try:
			for symbol in symbols:
				with open(DC.SYMBOL_ID_LIST, newline='') as csvfile:
					reader = csv.DictReader(csvfile)
					for row in reader:
						if (row['Symbol'] == symbol):
							url = self.asc.get_anime(row['ADB_ID'],ASC.PIC)
				if not filepath:
					filename = DC.PIC_FILEPATH_DEFAULT + symbol + '_pic.jpg'
				else:
					filename = filepath + symbol + '.jpg'
				urllib.request.urlretrieve(url,filename)
				rand_time = randint(1,40) # Add random time to avoid scrape bans
				time.sleep(DC.SLEEP_TIME+rand_time)
		except:
			raise
			
	def _getData(self,symbol):
		'''
		PRIVATE FUNCTION
		
		Gets data for a symbol.
		
		Keyword arguments:
			symbol -- string; Three (3), four (4) or five (5) 'ALL CAPS' letter string that represents an anime's rating tracking.
			
		Return:
			data -- list; List of data (users & scores) to be written. Format seen below:
				data = {'Users': int, 'MAL_Score': float, 'ADB_Rating': float, 'ADB_Average': float, 'ANN_Weighted': float}
		'''
		try:
			with open(DC.SYMBOL_ID_LIST, newline='') as csvfile:
				reader = csv.DictReader(csvfile)
				for row in reader:
					if (row['Symbol'] == symbol):
						msg = 'Loading data for ' + symbol + '...'
						print(msg)
						mal_data = self.asc.get_anime(row['MAL_ID'],ASC.MAL)
						adb_data = self.asc.get_anime(row['ADB_ID'],ASC.ADB)
						ann_data = self.asc.get_anime(row['ANN_ID'],ASC.ANN)
						data = {
							'Users': 0 if not mal_data else mal_data['Users'] ,
							'MAL_Score': 0 if not mal_data else mal_data['Score'],
							'ADB_Rating': 0 if not adb_data else adb_data['Weighted'],
							'ADB_Average': 0 if not adb_data else adb_data['Average'],
							'ANN_Weighted': 0  if not ann_data else ann_data['Weighted'],
							}
						return data
				return None
		except:
			raise
			
	def _writeLine(self,symbol,data,date=None):
		'''
		PRIVATE FUNCTION
		
		Writes a single entry line into the symbol's *.csv file at the corresponding date.
		If no date is given, the date is set to today's date.
		
		Keyword arguments:
			symbol -- string; Three (3), four (4) or five (5) 'ALL CAPS' letter string that represents an anime's rating tracking.
			data -- list; List of data (users & scores) to be written. Format seen below:
				data = {'Users': int, 'MAL_Score': float, 'ADB_Rating': float, 'ADB_Average': float, 'ANN_Weighted': float}
			date -- string; YYYY-MM-DD. If no date given, date set to today's date.
			
		Return:
			DC.ERROR -- int; An exception has occurred within this function.
			DC.INVALID -- int; Invalid arguments given.
		'''
		# ***** Check for invalid arguments *****
		if (len(symbol) > 5 or len(symbol) < 3):
			return DC.INVALID
			
		if not symbol.isupper():
			return DC.INVALID
		
		if not (len(data) == 5):
			return DC.INVALID
		
		# ***** End invalid argument checks *****
		
		# If no date given, use today's date
		if not (date):
			date = time.strftime('%m/%d/%Y')
			date = date.lstrip('0')
		# If date given, check if valid format
		else:
			r = re.compile('.*/.*/.*')
			if r.match(date) is None:
				return DC.INVALID
		
		try:
			write_this = [date,data['Users'],'%.2f'%data['MAL_Score'],'%.2f'%data['ADB_Rating'],'%.2f'%data['ADB_Average'],'%.3f'%data['ANN_Weighted']]
		except:
			return DC.INVALID
			
		# If last line is same date as 'date', perform writeover, else write a new line
		filename = 'stocks\\' + symbol + '.csv'
		# lastline = self._get_last_row(filename)
		# listed_date = lastline[0]
		# if (listed_date == date):
			# last_line_today = True
		# else:
			# last_line_today = False
			
		try:
			with open(filename, 'a', newline='') as csvfile:
				writer = csv.writer(csvfile)
				# if (last_line_today):
					# None
				# else:
				writer.writerow(write_this)
		except:
			return DC.ERROR
			
	def _get_last_row(self,csv_filename):
		'''
		PRIVATE FUNCTION
		<DEPRECATED>
		
		Return the last line in a *.csv file.
		
		Keyword arguments:
			csv_filename -- string;
			
		Return:
			row
		'''
		with open(csv_filename,'r') as f:
			reader = csv.reader(f)
			for line in reader:
				lastline = line
			return lastline
			
	def findIDs(self):
		return None
		
	def addSymbol(self,symbol,id_dict,year,season):
		'''addSymbol(self,symbol,year,season,id_dict) - Add a new symbol to the ID_LIST.'''
		try:
			write_this = [symbol,year,season,id_dict['MAL_ID'],id_dict['ADB_ID'],id_dict['ANN_ID']]
		except:
			return DC.INVALID
		
		try:
			with open(DC.SYMBOL_ID_LIST,'a',newline='') as csvfile:
				writer = csv.writer(csvfile)
				writer.writerow(write_this)
		except:
			return DC.ERROR

	def createNewCSV(self,symbol):
		'''createNewCSV(self,symbol) - Create a new *.csv file for a symbol if file doesn't exist.'''
		# ***** Check for invalid arguments *****
		if (len(symbol) > 5 or len(symbol) < 3):
			return DC.INVALID
			
		if not symbol.isupper():
			return DC.INVALID
		
		# ***** End invalid argument checks *****
		
		write_this = { 'Date' : 0, 'Users': 1, 'MAL_Score': 2, 'ANIDB_Weighted': 3,'ANIDB_Avg': 4, 'ANN_Score': 5 }
		filename = 'stocks\\' + symbol + '.csv'
		if os.path.isfile(filename):
			return None
		try:
			with open(filename,'w') as csvfile:
				writer = csv.DictWriter(csvfile, write_this.keys())
				writer.writeheader()
		except:
			return DC.ERROR