import urllib.request

from malparser.anime import Anime
from malparser.manga import Manga

HEADERS = {
	"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
	"Accept-Language": "en",
	"User-Agent": 'api-indiv-E1751B9AA0959370CFDBEEBFD125A27D'
}

class MAL(object):

	def _fetch(self,obj):
		req = urllib.request.Request(obj._get_url(), None, HEADERS)
		data = urllib.request.urlopen(req).read()
		obj.parse(data.decode('utf-8'))

	def _handle_related(self, obj):
		related = {'manga': Manga, 'anime': Anime}
		
		for key, values in obj.related.items():
			obj.related[key] = [related[v['type']](v['id'],self) for v in values]
			
	def get_anime(self, mal_id):
		return Anime(mal_id, self)
		
	def get_manga(self, mal_id):
		return Manga(mal_id, self)